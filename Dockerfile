FROM node:14-stretch

RUN useradd -d /home/term -m -s /bin/bash term
RUN echo 'term:term' | chpasswd

RUN yarn global add wetty

ENTRYPOINT ["wetty"]
